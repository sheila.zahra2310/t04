class readystock extends kue{
    private double jumlah;

  public readystock (String name, double price, double jumlah) {
    super(name, price);
    this.jumlah = jumlah;
  }

  public double getJumlah() {
    return jumlah;
  }

  public void setJumlah(double jumlah) {
    this.jumlah = jumlah;
  }

  public double hitungHarga() {
    return getPrice() * jumlah * 2;
  }

  public double Berat() {
    return 0;
  }

  public double Jumlah() {
    return jumlah;
  }


  public String toString(){
    return String.format("readystock: "+super.toString()+"\njumlah          :"+jumlah+"\nprice           :"+hitungHarga());
    }
}
