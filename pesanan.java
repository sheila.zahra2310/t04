class pesanan extends kue{
    private double Berat;

    public pesanan (String name, double price, double Berat) {
        super(name, price);
        this.Berat = Berat;
      }
    public void setBerat (double Berat){
        this.Berat = Berat;
    }

    public double getBerat(){
        return Berat;
    }

    public double hitungHarga (){
        return getPrice() * Berat;
    }

    public double Berat(){
        return Berat;
    }

    public double Jumlah(){
        return 0;
    }

    public String toString(){
        return String.format("pesanan: "+super.toString()+"\nberat           :"+Berat+"\nprice           :"+hitungHarga());
        }

}


